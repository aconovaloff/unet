""" Full assembly of the parts to form the complete network """

import torch.nn.functional as F

from .unet_parts import *

class UNet(nn.Module):
    def __init__(self, n_channels, n_classes, bilinear=True):
        super(UNet, self).__init__()
        self.n_channels = n_channels
        self.n_classes = n_classes
#        self.bilinear = bilinear
#        self.inc = DoubleConv(n_channels, 64)
        self.padUL = torch.nn.ZeroPad2d((1,0,1,0))
        self.padRD = torch.nn.ZeroPad2d((0,1,0,1))
        self.down1 = DownBlock(n_channels, 64)
        self.down2 = DownBlock(64, 128)
        self.down3 = DownBlock(128, 256)
        self.down4 = DownBlock(256, 512)
        self.down5 = DownBlock(512, 512)
        self.down6 = DownBlock(512, 512)
        self.down7 = DownBlock(512, 512)
        self.drop = DropoutBlock(512,512)
        self.up1 = UpBlock(1024, 512)
        self.up2 = UpBlock(1024, 512)
        self.up3 = UpBlock(1024, 256)
        self.up4 = UpBlock(512, 128)
        self.up5 = UpBlock(256, 64)
        self.up6 = UpBlock(128, 32)
        self.out = TanhBlock(128,3)

    def forward(self, x):
        p = False
#        x1 = self.inc(x)
        x0 = self.down1(x)
        x0 = self.padUL(x0)
        if(p): print('x0', x0.size())
        x1 = self.down2(x0)
        x1 = self.padRD(x1)
        if(p): print('x1', x1.size())
        x2 = self.down3(x1)
        x2 = self.padUL(x2)
        if(p): print('x2', x2.size())
        x3 = self.down4(x2)
        x3 = self.padRD(x3)
        if(p): print('x3', x3.size())
        x4 = self.down5(x3)
        x4 = self.padUL(x4)
        if(p):print('x4', x4.size())
        x5 = self.down6(x4)
        x5 = self.padRD(x5)
        if(p):print('x5', x5.size())
        x6 = self.down7(x5)
        if(p):print('x6', x6.size())

        x = self.drop(x6)
        if(p):print('drop', x.size())#, 'up', x.size())
        x = self.up1(x, x5)
        if(p):print('xup1', x.size())
        x = self.up2(x, x4)
        if(p):print('xup2', x.size())
        x = self.up3(x, x3)
        if(p):print('xup3', x.size())
        x = self.up4(x, x2)
        if(p):print('xup4', x.size())
        x = self.up5(x, x1)
        if(p):print('xup5', x.size())
        x = self.out(x, x0)
        if(p):print('out', x.size())
        return x
class Discriminator (nn.Module):
    def __init__(self, n_channels, n_classes, bilinear=True):
        super(Discriminator, self).__init__()
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.padUL = torch.nn.ZeroPad2d((1,0,1,0))
        self.padRD = torch.nn.ZeroPad2d((0,1,0,1))
        self.down1 = DownBlock(n_channels, 64)
        self.down2 = DownBlock(64, 128)
        self.down3 = DownBlock(128, 256)
        self.down4 = DownBlock(256, 512)
        self.down5 = DownBlock(512, 512)
        self.down6 = DownBlock(512, 512)
        self.down7 = DownBlock(512, 512)
        self.flatten = nn.Flatten()
        self.fc = nn.Linear(512, 1) #this needs to be checked for proper dimensions

    def forward(self, x):
        x = self.down1(x)
        x = self.padUL(x)
        x = self.down2(x)
        x = self.padRD(x)
        x = self.down3(x)
        x = self.padUL(x)
        x = self.down4(x)
        x = self.padRD(x)
        x = self.down5(x)
        x = self.padUL(x)
        x = self.down6(x)
        x = self.padRD(x)
        x = self.down7(x)
        x = self.flatten(x)
        out = self.fc(x)
        return out


class UNet_o(nn.Module):
    def __init__(self, n_channels, n_classes, bilinear=True):
        super(UNet, self).__init__()
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.bilinear = bilinear

        self.inc = DoubleConv(n_channels, 64)
        self.down1 = Down(64, 128)
        self.down2 = Down(128, 256)
        self.down3 = Down(256, 512)
        factor = 2 if bilinear else 1
        self.down4 = Down(512, 1024 // factor)
        self.up1 = Up(1024, 512 // factor, bilinear)
        self.up2 = Up(512, 256 // factor, bilinear)
        self.up3 = Up(256, 128 // factor, bilinear)
        self.up4 = Up(128, 64, bilinear)
        self.outc = OutConv(64, 3)


    def forward(self, x):
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)
        logits = self.outc(x)
        return logits

class Discriminator_o (nn.Module):
    def __init__(self, n_channels, n_classes, bilinear=True):
        super(Discriminator, self).__init__()
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.bilinear = bilinear

        self.inc = DoubleConv(n_channels, 64)
        self.down1 = Down(64, 128)
        self.down2 = Down(128, 256)
        self.down3 = Down(256, 512)
        factor = 2 if bilinear else 1
        self.down4 = Down(512, 1024 // factor)

        self.flatten = nn.Flatten()
        self.fc = nn.Linear(1208320, 1)

    def forward(self, x):
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)

        logits = self.flatten(x5)
        out = self.fc(logits)
        return out

    #https://discuss.pytorch.org/t/transition-from-conv2d-to-linear-layer-equations/93850
    #https://machinelearningmastery.com/how-to-develop-a-conditional-generative-adversarial-network-from-scratch/
